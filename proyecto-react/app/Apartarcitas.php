<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartarcitas extends Model
{
    //
    protected $fillable = ["nombre", "apellido", "correo", "doctor", "fecha", "hora"];
}
