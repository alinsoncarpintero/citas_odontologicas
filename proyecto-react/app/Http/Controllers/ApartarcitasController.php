<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Apartarcitas;

class ApartarcitasController extends Controller
{
    //
    public function Store(Request $request) {
        $input = $request->all();
        $servi = Apartarcitas::create($input);
        return response()->json($servi);
    }
    public function GetCitas(){
        $datos['result'] = Apartarcitas::all();
        return response()->json($datos);
    }
}
