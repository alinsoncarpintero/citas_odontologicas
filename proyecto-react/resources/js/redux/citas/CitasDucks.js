import axion from 'axios'

// Constantes
  const DataInicial = {
      array:[]
  }

  //Type
   const GET_LISTA_SUCCESS = "GET_LISTA_SUCCESS"
   const POST_SUCCESS = "POST_SUCCESS"
   const DELETE_SUCCESS = "DELETE_SUCCESS"

   //Reducers esta parte maneja el crud
   const  Citas = (state = DataInicial, action) => {
        switch (action.type) {
            case GET_LISTA_SUCCESS:
                return {...state, array:action.payload}
            case POST_SUCCESS:
                return {...state, array:action.payload}
            case DELETE_SUCCESS:
                return {...state, array:action.payload}    


            default:
                return state
                
        }
   }

   export default Citas;

   //acciones
 export const ObtenerLista = () => async(dispatch, getState)=>{
        try {
            const respuesta = await axion.get('api/GetServicio')
            dispatch({
                type: GET_LISTA_SUCCESS,
                payload: respuesta.data.result
            })
        } catch (error) {
            
        }
   }

 export const Postapartarcita =  statusformu =>  async dispatch =>{
     try {
         const config = {
             headers:{
                 "Contect-type": "Aplication/json"
             }
         }

         const respuesta = await axion.post('api/PostApartarcitas', statusformu, config)
         dispatch(
             {
                type: POST_SUCCESS,
                payload: respuesta.data
             }
        )

     } catch (error) {
         console.log(error);
     }
 }

