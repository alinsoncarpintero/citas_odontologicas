import {createStore, combineReducers, compose, applyMiddleware} from  'redux'
import thunk from 'redux-thunk'
import Citas from '../../redux/citas/CitasDucks'

const rootReducer = combineReducers({
    citas :Citas 
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const getStorecita = () =>{
    const store = createStore (rootReducer, composeEnhancers(applyMiddleware(thunk)))
    return store
}

export default getStorecita;

