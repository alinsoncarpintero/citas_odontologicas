import React from 'react';
import ReactDOM from 'react-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      maxWidth: '100% !important',
      marginLeft: '0px !important',
      marginRigth: '0px !important',
      backgroundColor: '#f1f3f9',
      paddingTop: '72px',
    },
    paper: {

      width: '23rem',  
      padding: theme.spacing(3),
      textAlign: 'center',
      backgroundColor:'#fff',
      margin: 'auto',
      boxShadow: 'none',
      
    },
    hr:{
      boxShadow: 'none',
      border:  '1px solid #5dd8f9',   
      borderLeft: '3px solid #5dd8f9 !important',
      height: '15px',
      borderRadius: '0px',
      padding:'10px',
      fontSize:'11px',
      textAlign:'center',
      marginBottom:'20px',
    },
    titulo1:{
      textAlign: 'center',
      width: '100%',  
      borderBottom: '1px solid #5dd8f9',
      fontSize:'25px',
      marginBottom: '30px',
    },
    titulo2:{
        textAlign: 'center',
        width: '90% !important',  
        borderBottom: '1px solid #5dd8f9',
        fontSize:'25px',
        paddingTop:'5px',
        marginBottom: '30px !important',
        margin:'auto !important',
      },
    parrafo:{
        textAlign:'justify',
        marginTop:'35px',
        fontSize:'15px',
        color:'#797e80',
        marginBottom:'30px',
        lineHeight:'22px',
    },
    parrafo2:{
        textAlign:'justify',
        marginTop:'65px !important',
        fontSize:'16.5px',
        color:'#797e80',
        marginBottom:'60px !important',
        lineHeight:'22px',
    },
    formulario:{
        '& > *': {
            margin: theme.spacing(1),
            width: '34ch',
            marginBottom:'30px',
          }, 
          textAlign: 'center',
          backgroundColor:'#fff',
          margin: 'auto',
          boxShadow: 'none',
          paddingTop:'20px',
    },
    pie:{
         fontSize:'20px',
         marginTop:'30px !important',
         textAlign:'left !important',   
         marginBottom: '94px !important',
    },
 
  }));
    
    function Componente1() {  
        const classes = useStyles();
    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
        <Grid item xs={4} >
            <Paper className={classes.paper}>
            <p className={classes.titulo1}><b>Horario de Atencion</b></p>
                    <Grid container spacing={2}>
                     <Grid item xs={6}>
                         <Paper className={classes.hr}>
                                Lunes 8:00am - 7:00pm
                         </Paper>   
                     </Grid>
                     <Grid item xs={6}>
                         <Paper className={classes.hr}>
                                Martes 8:00am - 7:00pm
                         </Paper>   
                     </Grid>
                     <Grid item xs={6}>
                         <Paper className={classes.hr}>
                                Miercole 9:00am - 5:00pm
                         </Paper>   
                     </Grid>
                     <Grid item xs={6}>
                         <Paper className={classes.hr}>
                                Jueves 9:00am - 5:00pm
                         </Paper>   
                     </Grid>
                     <Grid item xs={6}>
                         <Paper className={classes.hr}>
                                Viernes 9:00am - 5:00pm
                         </Paper>   
                     </Grid>
                     <Grid item xs={6}>
                         <Paper className={classes.hr}>
                                Sabádos 9:00am - 5:00pm
                         </Paper>   
                     </Grid> 
                  </Grid>  
                  <p className={classes.parrafo}>
                  Que esperas!! Elige un horario de atencion y dirige te
                 a nuestros consultorios, recuerda que tu sonrisa es esencial.
                  </p>
            </Paper>
        </Grid> 
          <Grid container item xs={4} >
          <Paper className={classes.formulario}>
                 <p className={classes.titulo2}><b>Contactános</b></p>
                <TextField id="outlined-basic" label="Nombre" variant="outlined" />                  
                <TextField id="outlined-basic" label="Apellido" variant="outlined" />                  
                <TextField id="outlined-basic" label="Correo" variant="outlined" />  
                <Button variant="contained" color="primary">
                    Enviar
                </Button>                
            </Paper>
          </Grid>
          <Grid container item xs={4} >
          <Paper className={classes.paper}>
          <p className={classes.titulo2}><b>Caso de emergencia</b></p>
                  <p className={classes.parrafo2}>
                  Nuestra clínica brinda servicios dentales de 
                  alto nivel, ofrece soluciones integrales para el 
                  tratamiento de cualquier enfermedad dental.
                  </p>
                  <p className={classes.pie}>
                        <b>1-800-1234-567<br></br>
                        ¡Llámanos!</b>
                  </p>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }

  export default Componente1;
  if (document.getElementById('info')){
      ReactDOM.render(<Componente1/>, document.getElementById('info'));

  }

  