import React from 'react';
import ReactDOM from 'react-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Formulario from './Formulario/Formulario';
import Grid from '@material-ui/core/Grid';
import {Provider} from 'react-redux';
import getStorecita from '../redux/citas/Store';



const useStyles = makeStyles((theme) => ({
    
    root:{
      maxWidth: '100% !important',  
      marginLeft: '0px !important',
      marginRigth: '0px !important',
      backgroundColor: '#f7f7f8 ',
      paddingLeft: '30px',
      paddingRight: '30px',
      paddingTop:'70px',

    },
    ds:{
        width: '100%',
        margin: '0px',
    },
    col: {
        '& > *': {
            margin: theme.spacing(1),
            width: '24ch',
          },
        width: '35rem',  
        padding: theme.spacing(2),
        textAlign: 'center',
        borderRadius: '15px',
        backgroundColor: '#fff',

    },
    paper: {
        '& > *': {
            margin: theme.spacing(1),
            width: '24ch',
        
          },
        padding: theme.spacing(2),
        textAlign: 'center',
        backgroundColor: '#f7f7f8',

    },
    titulo:{
        textAlign:'center ',
        fontSize:'20px',
        width:'100%',

    },
    bot:{
        backgroundColor:'#3c54d8  !important',
        color:'#fff',
        '&:hover': { 
            backgroundColor:'#0128ff  !important',
            color:'#fff',
        },
    },

        

}));

function CItas(){
    const store = getStorecita()
    const classes = useStyles();
    const f = <Formulario/>
        return(
            <div className={classes.root}> 
                  <Grid container spacing={2} className={classes.ds}>
                      <Grid item xs={6}>
                          <Paper className={classes.col}>
                          <p className={classes.titulo}>
                                <b> Apartar citas </b>
                          </p>
                               <Provider store = {store}>
                                    {f}  
                               </Provider>  
                                 
                          </Paper>
                      </Grid>  
                      <Grid item xs={6} >
                          <Paper className={classes.paper}>
                                <img src="../../img/calendario (1).png"></img>
                          </Paper>
                      </Grid> 
                  </Grid>  
            </div>
        );
}

export default CItas;