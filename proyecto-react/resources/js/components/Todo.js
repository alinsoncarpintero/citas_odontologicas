import React from 'react';
import ReactDOM from 'react-dom';
import Componente1 from './Componente1';
import Componente2 from './Componente2';
import Footer from './Footer';


function Todo(){
    const c1 = <Componente1/>
    const c2 = <Componente2/>
    const f = <Footer/>
    return(
        <div>
            {c1}
            {c2}
            {f}
        </div>
    );
}

export default Todo;

if (document.getElementById('todo')) {
    ReactDOM.render(<Todo />, document.getElementById('todo'));
}
