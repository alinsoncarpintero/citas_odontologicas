import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { FormControl, InputLabel, Input, FormHelperText, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {Postapartarcita} from "../../redux/citas/CitasDucks";
import { connect } from "react-redux";
import TextField from '@material-ui/core/TextField'; 

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(2),
            width:'15rem',
            margin:'auto',

        },
    },
    for:{
        '& > *': {
            margin: theme.spacing(2),
            width: '24ch',
          },
          margin:'auto',
          width:'34rem',
    },
    formu:{
        width:'34rem',
    },
}));
const Formulario = ({Postapartarcita}) => {
    const classes = useStyles();
    const [stateFormulario, setstateFormulario] = useState({
        nombre: "",
        apellido: "",
        correo: "",
        doctor: "",
        fecha: "",
        hora: ""
    })

    const cambios = e => {
        const { name, value } = e.target
        setstateFormulario({
            ...stateFormulario,
            [name]: value
        })

        console.log("Hola ", name);
    }
    const enviar = (e) => {
        e.preventDefault();
        Postapartarcita(stateFormulario);
       setstateFormulario({
        nombre: "",
        apellido: "",
        correo: "",
        doctor: "",
        fecha: "",
        hora: ""
        })

    }
    const boton = <div className={classes.root}>
        <Button variant="contained" color="primary" onClick={e => enviar(e)} >
            Guardar
    </Button>
    </div>
    const formularios = <div>
        <div className={classes.formu}>
            <form onSubmit=" return false;" >

                <div className={classes.for}>
                    <FormControl>
                        <TextField name="nombre" id="outlined-basic" label="Nombre" variant="outlined"  onChange={e => cambios(e)} />
                    </FormControl>
                    <FormControl>                        
                        <TextField name="apellido" id="outlined-basic" label="Apellido" variant="outlined"  onChange={e => cambios(e)} />
                    </FormControl>

                    <FormControl>                        
                        <TextField name="correo" id="outlined-basic" label="Correo" variant="outlined"  onChange={e => cambios(e)} />
                    </FormControl>
                    <FormControl>
                        <TextField name="doctor" id="outlined-basic" label="Doctor" variant="outlined"  onChange={e => cambios(e)} />
                    </FormControl>
                    <FormControl>         
                        <TextField name="fecha" id="outlined-basic" label="Fecha" variant="outlined"  onChange={e => cambios(e)} />
                    </FormControl>
                    <FormControl>
                        <TextField name="hora" id="outlined-basic" label="Hora" variant="outlined"  onChange={e => cambios(e)} />
                    </FormControl>

                </div>
                {boton}
            </form>
        </div>

    </div>


    return (
        <div>
            {formularios}

        </div>
    )
}

Formulario.propTypes = {
    Postapartarcita: PropTypes.func.isRequired
}

export default connect (null, {Postapartarcita}) (Formulario)
