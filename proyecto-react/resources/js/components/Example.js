import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header'



function Example() {
    const h = <Header/>
    return (
        <div className="container">
            <div className="menu">
                 {h}
            </div>

        </div>
    );
}

export default Example;

if (document.getElementById('header')) {
    ReactDOM.render(<Example />, document.getElementById('header'));
}
