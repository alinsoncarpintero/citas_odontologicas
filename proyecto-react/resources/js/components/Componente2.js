import React from 'react';
import ReactDOM from 'react-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      maxWidth: '100% !important',
      marginTop:'100px !important',
      marginLeft: '0px !important',
      marginRigth: '0px !important',
      
    },
    paper: {

      width: '18rem',  
      padding: theme.spacing(3),
      backgroundColor:'#fff',
      margin: 'auto',
      padding:'0px !important',
      boxShadow: '0 0 10px 1px rgb(0,0,0,.2) !important',
      borderRadius:'3px',
      
    },
    titulo:{
       fontSize: '1.25rem', 
       textAlign:'center',
       marginTop:'10px !important',
       marginBottom:'10px',
       fontWidth:'500px',
       color:'#000',
       
    },
    parrafo:{
        textAlign:'justify !important',
        padding:'15px !important', 
        fontSize:'13px', 
        lineHeight:'22px',
        color:'#000',
    },
    ima:{
       width:'100%',     
    },
 
  }));

  function Componente2(){
    const classes = useStyles();
    return(
    <div className={classes.root}>
          <Grid container spacing={3}>
              <Grid item xs={4}>
                   <Paper className={classes.paper}>
                       <img className={classes.ima} src="../../img/pediatric.jpg"></img>
                       <p className={classes.titulo}>
                            <b>Odontología Pediatrica</b>
                       </p>
                       <p className={classes.parrafo}>
                        Es la especialidad de la odontología responsable del diagnóstico, prevención, control y 
                        tratamiento de problemas orales en niños como: caries, enfermedad de las encías, etc.
                       </p>
                   </Paper> 
              </Grid>
              <Grid item xs={4}>
                   <Paper className={classes.paper}>
                       <img className={classes.ima} src="../../img/Endodoncia.jpg"></img>
                       <p className={classes.titulo}>
                           <b> Endodoncia</b>
                       </p>
                       <p className={classes.parrafo}>
                        La endodoncia es una especialidad de odontología que se encarga 
                        del diagnóstico, tratamiento y rehabilitación de patologías que
                         comprometen el tejido pulpar del diente (nervio).
                       </p>
                   </Paper> 
              </Grid>
              <Grid item xs={4}>
                   <Paper className={classes.paper}>
                       <img className={classes.ima} src="../../img/implante.jpg"></img>
                       <p className={classes.titulo}>
                           <b> implantes dentales</b>
                       </p>
                       <p className={classes.parrafo}>
                        Los implantes dentales son una excelente opción para aquellos que han perdido un diente.
                        Reemplazan la raíz del diente y sirven de soporte para estabilizar las coronas.
                       </p>
                   </Paper> 
              </Grid>
              <Grid item xs={4}>
                   <Paper className={classes.paper}>
                       <img className={classes.ima} src="../../img/DENTIS2.jpg"></img>
                       <p className={classes.titulo}>
                            <b>Odontología Cosmetica</b>
                       </p>
                       <p className={classes.parrafo}>
                       Si usted tiene dientes rotos, desiguales o manchados, la odontología cosmética puede ayudarle. 
                       La odontología cosmética es diferentedel tratamiento de ortodoncia, 
                       que puede enderezar los dientes con frenillos u otros dispositivos
                       </p>
                   </Paper> 
              </Grid>
              <Grid item xs={4}>
                   <Paper className={classes.paper}>
                       <img className={classes.ima} src="../../img/blanqueamiento.jpg"></img>
                       <p className={classes.titulo}>
                            <b>Blanqueamiento Dental</b>
                       </p>
                       <p className={classes.parrafo}>
                         La limpieza dental consiste en restaurar el color y la translucidez de los dientes mediante la eliminación
                        de manchas y pigmentaciones que se adquieren con el tiempo.Existen diferentes métodos para hacerlo.
                       </p>
                   </Paper> 
              </Grid>
              <Grid item xs={4}>
                   <Paper className={classes.paper}>
                       <img className={classes.ima} src="../../img/limpieza.jpg"></img>
                       <p className={classes.titulo}>
                           <b> limpieza dental</b>
                       </p>
                       <p className={classes.parrafo}>
                        La limpieza dental es parte de la higiene oral e involucra la remoción
                        de la placa dental de los dientes con la intención de prevenir cavidades 
                        ( caries), gingivitis, y enfermedades peridontales.
                       </p>
                   </Paper> 
              </Grid>
          </Grid>  
    </div>
    ); 
  }

  export default Componente2;
  if (document.getElementById('servicio')){
      ReactDOM.render(<Componente2/>, document.getElementById('servicio'));
  } 