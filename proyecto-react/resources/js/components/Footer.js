import React from 'react';
import ReactDOM from 'react-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      maxWidth: '100% !important',
      marginTop:'100px !important',
      marginLeft: '0px !important',
      marginRigth: '0px !important',
      backgroundColor:'#11c5f5',
      height:'300px',
      
    },
    paper: {

        padding: theme.spacing(3),
        margin: 'auto',
        padding:'0px !important',
        boxShadow: 'none',
        backgroundColor:'#11c5f5',
      },
      titulo:{
          marginTop:'20px !important',
          color:'#fff',
          fontSize:'20px',
          textAlign:'left',
          marginBottom:'30px',
      },
      mf:{
          marginRight:'10px !important',
          maxWidth:'30px !important',
      },
      fi:{
          color:'#fff',
          paddingLeft:'5px',
           maxWidth:'95% !important',
      },
      fc:{
          marginTop:'30px !important',
          marginBottom:'30px !important',
          color:'#fff',
      },
}));

function Footer(){
    const classes = useStyles();
    return(
        <div className={classes.root}>
            <Grid container spacing={3}>
                    <Grid item xs={4}>
                        <Paper className={classes.paper}>

                        </Paper>
                    </Grid>
                    <Grid item xs={4}>
                        <Paper className={classes.paper}>
                               <p className={classes.titulo}>
                                    <b>ODONTOLOGIA ALCAR</b>
                               </p> 

                                  <p className={classes.fi}><img  src="../../img/inicio.png"/> Inicio</p>
                                  <br/>
                                
                                  <p className={classes.fi}><img  src="../../img/dentista.png"/> Clinicas Odontologicas</p>
                                  <br/>
                                  
                                  <p className={classes.fi}><img  src="../../img/idea.png"/> Sugerencias</p>
                                  <br/>
                                  
                                  <p className={classes.fi}><img  src="../../img/bolso.png"/> Politicas de tratamiento de datos</p>
                                  <br/>
                              
                        </Paper>
                    </Grid>
                    <Grid item xs={4}>
                        <Paper className={classes.paper}>
                                <p className={classes.titulo}>
                                    <b>Siguenos</b>

                               </p> 
                               <img className={classes.mf} src="../../img/twitter.png"></img>  
                               <img className={classes.mf} src="../../img/facebook.png"></img>  
                               <img className={classes.mf} src="../../img/instagram.png"></img>  
                               <img className={classes.mf} src="../../img/logo.png"></img>
                               <p className={classes.fc}>
                                    <b>Contáctanos</b>
                               </p>
                               <p className={classes.fi}><img  src="../../img/gmail.png"/>  alinsoncarpintero@gmail.com</p>
                                  <br/>  
                        </Paper>
                    </Grid>
            </Grid>
        </div>
    );
}

export default Footer;
if(document.getElementById('')){
    ReactDOM.render(<Footer/>, document.getElementById(''));
}