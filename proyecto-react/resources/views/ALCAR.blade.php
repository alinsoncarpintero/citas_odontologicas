<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="../css/estilos.css">
    <link href="https://fonts.googleapis.com/css2?family=Kumbh+Sans:wght@300&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Citas Odontológicas</title>
</head>
<body>
     <div id="header"> </div>
     <div >
     <div class="slider">
			<ul>
				<li>
  <img src="../../img/dental1.jpg" alt="">
 </li>
				<li>
  <img src="../../img/dental.jpg" alt="">
</li>
				<li>
  <img src="../../img/odon.jpg" alt="">
</li>
				<li>
  <img src="../../img/dental1.jpg" alt="">
</li>
			</ul>
		</div>
     </div>
     <div id="todo"> </div>
</body>
<footer>
</footer>
<script src="js/app.js"></script>

</html>